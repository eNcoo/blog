<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'category_id' => $faker->numberBetween(1, 2),
        'title' => $faker->unique()->words(5, true),
        'preview' => $faker->realText(120, 2),
        'content' => $faker->paragraphs(10, true),
        'image_url' => './images/img_1.jpg'
    ];
});
