<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = (int)$this->command->ask('How many posts do you need ?', 10);

        $this->command->info("Creating {$count} posts.");

        factory(App\Post::class, $count)->create();

        $this->command->info('Posts Created!');

    }
}
