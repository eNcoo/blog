<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Blog</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700|Playfair+Display:400,700,900" rel="stylesheet">

    <link rel="stylesheet" href="/fonts/icomoon/style.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="/css/aos.css">

    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

<div class="site-wrap">

    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar pt-3" role="banner">
        <div class="container-fluid">
            <div class="row align-items-center">

                <div class="col-6 col-xl-6 logo">
                    <h1 class="mb-0"><a href="index.html" class="text-black h2 mb-0">Půlročníková práce - Blog</a></h1>
                </div>


                <div class="col-12 d-none d-xl-block border-top">
                    <nav class="site-navigation text-center " role="navigation">

                        <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block mb-0"
                            style="padding-left: 0px !important;">
                            <li><a href="{{ url('/') }}">Domů</a></li>
                            <li class="has-children">
                                <a href="javascript:void(0)">Kategorie</a>
                                <ul class="dropdown">
                                    @foreach(App\Category::all() as $category)
                                        <li><a href="{{ url('/posts/category/' . $category->url) }}">{{ $category->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li><a href="{{ url('/posts') }}">Poslední</a></li>
                            <li><a href="{{ url('/admin') }}">Administrace</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
</div>

@yield('content')

<div class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p style="margin-bottom: 0px;">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;
                    {{ date('Y') }}
                    Všechna práva vyhrazena | Předloha této stránky je dílem <a
                            href="https://colorlib.com" target="_blank">Colorlib</a>. Stránku naprogramoval <a
                            href="http://rothadam.cz" target="_blank">Adam Roth</a>.
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
            </div>
        </div>
    </div>
</div>

</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/aos.js"></script>

<script src="js/main.js"></script>

</body>
</html>