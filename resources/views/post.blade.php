@extends('layouts.app')

@section('content')
    <div class="site-cover site-cover-sm same-height overlay single-page"
         style="background-image: url('{{ $post->image_url }}');">
        <div class="container">
            <div class="row same-height justify-content-center">
                <div class="col-md-12 col-lg-10">
                    <div class="post-entry text-center">
                        <span class="post-category text-white bg-{{ $post->category->color }} mb-3">{{ $post->category->name }}</span>
                        <h1 class="mb-4"><a href="#">{{ $post->title }}</a></h1>
                        <div class="post-meta align-items-center text-center">
                            <figure class="author-figure mb-0 mr-3 d-inline-block"><img src="https://gravatar.com/avatar/{{ md5(strtolower(trim($post->user->email))) }}"
                                                                                        alt="Image" class="img-fluid">
                            </figure>
                            <span class="d-inline-block mt-1">By {{ $post->user->name }}</span>
                            <span>&nbsp;-&nbsp; {{ $post->created_at->format('F j, Y') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="site-section py-lg">
        <div class="container">

            <div class="row blog-entries element-animate justify-content-center">

                <div class="col-md-12 col-lg-8 main-content">

                    <div class="post-content-body">
                        {!! $post->content !!}
                    </div>


                    <div class="pt-5">
                        <p>Tagy:&nbsp;
                        @php
                            $hashtags = "";
                            foreach ($post->tags as $tag) {
                                $hashtags = $hashtags . '<a href="'. url('/posts/tag/' . $tag->name) .'">#'. $tag->name .'</a>, ';
                            }
                            $hashtags = rtrim($hashtags, ', ');
                            echo $hashtags;
                        @endphp
                    </div>


                </div>

                <!-- END main-content -->

            </div>
        </div>
    </section>
@endsection