@extends('layouts.app')

@section('content')

    @if(\App\Post::all()->count() > 1)
        <div class="slide-one-item home-slider owl-carousel">
            @endif
            @foreach(\App\Post::orderBy('id', 'DESC')->limit(3)->get() as $post)
                <div class="site-cover site-cover-sm same-height overlay"
                     style="background-image: url('{{ url($post->image_url) }}');">
                    <div class="container">
                        <div class="row same-height align-items-center">
                            <div class="col-md-12 col-lg-6">
                                <div class="post-entry">
                                    <span class="post-category text-white bg-{{ $post->category->color }} mb-3">{{ $post->category->name }}</span>
                                    <h2 class="mb-4"><a href="{{ url('/post/' . $post->id) }}">{{ $post->title }}</a></h2>
                                    <div class="post-meta align-items-center text-left">
                                        <figure class="author-figure mb-0 mr-3 float-left"><img
                                                    src="https://gravatar.com/avatar/{{ md5(strtolower(trim($post->user->email))) }}"
                                                    alt="Image" class="img-fluid">
                                        </figure>
                                        <span class="d-inline-block mt-1">By {{ $post->user->name }}</span>
                                        <span>&nbsp;-&nbsp; {{ $post->created_at->format('F j, Y') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            @if(\App\Post::all()->count() > 1)
        </div>
    @endif

    <div class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="rounded border p-4">
                        <div class="row align-items-stretch">
                            @foreach(\App\Post::orderBy('id', 'DESC')->limit(3)->get() as $post)
                                <div class="col-md-6 col-lg-4 mb-3 mb-lg-0">
                                    <a href="{{ url('/post/' . $post->id) }}" class="d-flex post-sm-entry">
                                        <figure class="mr-3 mb-0"><img src="{{ $post->image_url }}" alt="Image"
                                                                       class="rounded"></figure>
                                        <div>
                                            <span class="post-category bg-{{ $post->category->color }} text-white m-0 mb-2">{{ $post->category->name }}</span>
                                            <h2 class="mb-0">{{ $post->title }}</h2>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                            @if(\App\Post::count() < 3)
                                @for($i = 3 - \App\Post::count(); $i > 0; $i--)
                                    <div class="col-md-6 col-lg-4 mb-3 mb-lg-0">
                                    <span class="d-flex post-sm-entry">
                                        <figure class="mr-3 mb-0"><img src="./images/no_content.jpg" alt="Image"
                                                                       class="rounded"></figure>
                                    </span>
                                    </div>
                                @endfor
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section">
        <div class="container">
            <div class="row">

                @foreach(App\Category::all() as $category)
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <div class="section-heading mb-5 d-flex align-items-center">
                            <h2>{{ $category->name }}</h2>
                            <div class="ml-auto"><a href="{{ url('/posts/' . $category->url) }}"
                                                    class="view-all-btn">View
                                    All</a></div>
                        </div>
                        @if($category->posts->count() < 1)
                            <div class="entry2 mb-5">
                                <a href="single.html"><img src="{{ url('/images/no_content.jpg') }}" alt="Image"
                                                           class="img-fluid rounded"></a>
                                <span class="post-category text-white bg-{{ $category->color }} mb-3">{{ $category->name }}</span>
                                <h2><a href="single.html">Žádné články</a></h2>
                                <div class="post-meta align-items-center text-left clearfix">
                                </div>
                                <p>Tato kategorie zatím neobsahuje žádné články.</p>
                            </div>
                        @else
                            @php
                                $post = $category->posts->last();
                            @endphp
                            <div class="entry2 mb-5">
                                <a href="{{ url('/post' . $post->id) }}"><img src="{{ url($post->image_url) }}" alt="Image"
                                                           class="img-fluid rounded"></a>
                                <span class="post-category text-white bg-{{ $category->color }} mb-3">{{ $category->name }}</span>
                                <h2><a href="{{ url('/post/' . $post->id) }}">{{ $post->title }}</a></h2>
                                <div class="post-meta align-items-center text-left clearfix">
                                    <figure class="author-figure mb-0 mr-3 float-left"><img
                                                src="https://gravatar.com/avatar/{{ md5(strtolower(trim($post->user->email))) }}"
                                                alt="Image"
                                                class="img-fluid"></figure>
                                    <span class="d-inline-block mt-1">By {{ $post->user->name }}</span>
                                    <span>&nbsp;-&nbsp; {{ $post->created_at->format('F j, Y') }}</span>
                                </div>
                                <p>{{ $post->preview }}</p>
                            </div>
                            @if($category->posts->count() > 1)
                                @php($tmp_posts = $category->posts->sortByDesc('id')->slice(1)->take(3))
                                @foreach($tmp_posts as $post)
                                    <div class="entry4 d-block d-sm-flex">
                                        <figure style="position: absolute; right: 16px;" class="figure order-2"><a href="{{ url('/post/' . $post->id) }}"><img
                                                        src="{{ url($post->image_url) }}"
                                                        alt="Image"
                                                        class="img-fluid rounded" width="100" height="56"></a>
                                        </figure>
                                        <div class="text mr-4 order-1">
                                            <span class="post-category text-white bg-{{ $category->color }} mb-3">{{ $category->name }}</span>
                                            <h2>
                                                <a href="{{ url('/post/' . $post->id) }}">{{ $post->title }}</a>
                                            </h2>
                                            <span class="post-meta mb-3 d-block">{{ $post->created_at->format('F j, Y') }}</span>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection