@extends('layouts.app')

@section('content')
    <section class="site-section py-lg" style="padding: 0 !important">
        <div class="container">

            <div class="row justify-content-center blog-entries element-animate mb-5">

                <div class="col-md-12 col-lg-8 main-content">
                    @if($posts->count() < 1)
                        <br><div style="text-align: center;">Tato kategorie zatím neobsahuje žádné články.</div>
                    @endif
                    @foreach($posts as $post)
                        <div class="entry2 mb-5">
                            <a href="{{ url('/post/' . $post->id) }}"><img src="{{ url($post->image_url) }}" alt="Image"
                                                       class="img-fluid rounded"></a>
                            <span class="post-category text-white bg-{{ $post->category->color }} mb-3">{{ $post->category->name }}</span>
                            <h2><a href="{{ url('/post/' . $post->id) }}">{{ $post->title }}</a></h2>
                            <div class="post-meta align-items-center text-left clearfix">
                                <figure class="author-figure mb-0 mr-3 float-left"><img src="https://gravatar.com/avatar/{{ md5(strtolower(trim($post->user->email))) }}"
                                                                                        alt="Image"
                                                                                        class="img-fluid"></figure>
                                <span class="d-inline-block mt-1">By <a href="#">{{ $post->user->name }}</a></span>
                                <span>&nbsp;-&nbsp; {{ $post->created_at->format('F j, Y') }}</span>
                            </div>
                            <p>{{ $post->preview }}</p>
                        </div>
                    @endforeach

                    @if($posts->total() > 1)
                        <div class="row text-center pt-5 border-top">
                            <div class="col-md-12">
                                {{ $posts->links() }}
                            </div>
                        </div>
                    @endif

                </div>

                <!-- END main-content -->

            </div>
        </div>
    </section>
@endsection