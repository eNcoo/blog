@extends('layouts.admin')

@section('title', 'Správa kategorií')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Správa kategorií</h1>
        </div>

        <div class="row">
            @if(session()->has('message'))
                <div class="col-12">
                    <div class="card mb-4 py-3 border-left-{{ session()->get('alert-class') }}">
                        <div class="card-body">
                            {{ session()->get('message') }}
                        </div>
                    </div>
                </div>
            @endif
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Seznam kategorií</h6>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Název</th>
                                <th scope="col">Třída</th>
                                <th scope="col">Smart URL</th>
                                <th scope="col" class="text-right">Akce</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(App\Category::all()->count() < 1)
                                <td colspan="5">Zatím neexistuje žádná kategorie. Je třeba ji vytvořit.</td>
                            @endif
                            @foreach(App\Category::all() as $category)
                                <tr>
                                    <th scope="row">{{ $category->id }}</th>
                                    <td class="text-{{$category->color}}">{{ $category->name }}</td>
                                    <td>{{ $category->color }}</td>
                                    <td><a href="{{ url('/posts/category/' . $category->url) }}" target="_blank"
                                           class="text-muted">/posts/category/{{ $category->url }}</a></td>
                                    <td class="text-right">
                                        <a href="javascript:void(0);"
                                           class="catEditBtn"
                                           data-id="{{ $category->id }}"
                                           data-name="{{ $category->name }}"
                                           data-color="{{ $category->color }}"
                                           data-url="{{ $category->url }}"
                                        ><i
                                                    class="fa fa-search text-primary"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12" @if(!$errors->any()) style="display: none;" @endif id="catEdit">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Úprava kategorie</h6>
                            </div>
                            <div class="card-body">
                                <form method="post" action="{{ route('admin.categories') }}">
                                    @csrf
                                    @method('patch')
                                    <input type="hidden" id="id_e" name="id_e" value="{{ old('id_e') }}">
                                    <div class="form-group">
                                        <label for="name_e">Název kategorie</label>
                                        <input type="text" id="name_e" value="{{ old('name_e') }}" name="name_e"
                                               class="form-control{{ $errors->has('name_e') ? ' is-invalid' : '' }}">
                                        @if ($errors->has('name_e'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('name_e') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="color_e">Třída kategorie
                                            <small class="text-muted">(například: danger)</small>
                                        </label>
                                        <input type="text" id="color_e" value="{{ old('color_e') }}" name="color_e"
                                               class="form-control{{ $errors->has('color_e') ? ' is-invalid' : '' }}">
                                        @if ($errors->has('color_e'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('color_e') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="url_e">Smart URL</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">/posts/category/</span>
                                            </div>
                                            <input type="text" id="url_e" value="{{ old('url_e') }}" name="url_e"
                                                   class="form-control{{ $errors->has('url_e') ? ' is-invalid' : '' }}"
                                                   aria-describedby="basic-addon1">
                                            @if ($errors->has('url_e'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('url_e') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group text-right">
                                        <button class="btn btn-primary">Upravit kategorii</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Vytvoření nové kategorie</h6>
                    </div>
                    <div class="card-body">

                        <form method="post" action="{{ route('admin.categories') }}">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="name">Název kategorie</label>
                                <input type="text" id="name" name="name"
                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}">
                                @if ($errors->has('name'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('name') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="color">Třída kategorie
                                    <small class="text-muted">(například: danger)</small>
                                </label>
                                <input type="text" id="color" name="color"
                                       class="form-control{{ $errors->has('color') ? ' is-invalid' : '' }}">
                                @if ($errors->has('color'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('color') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="url">Smart URL</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">/posts/category/</span>
                                    </div>
                                    <input type="text" id="url" name="url"
                                           class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}"
                                           aria-describedby="basic-addon1">
                                    @if ($errors->has('url'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('url') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group text-right">
                                <button class="btn btn-primary">Vytvořit kategorii</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('.catEditBtn').on('click', function () {
            $('#id_e').val($(this).data('id'));
            $('#name_e').val($(this).data('name'));
            $('#color_e').val($(this).data('color'));
            $('#url_e').val($(this).data('url'));

            $('#catEdit').show(300);
        });
    </script>
@endsection