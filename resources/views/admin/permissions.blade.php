@extends('layouts.admin')

@section('title', 'Správa oprávnění')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Správa oprávnění</h1>
        </div>

        <div class="row">
            @if(session()->has('message'))
                <div class="col-12">
                    <div class="card mb-4 py-3 border-left-{{ session()->get('alert-class') }}">
                        <div class="card-body">
                            {{ session()->get('message') }}
                        </div>
                    </div>
                </div>
            @endif
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Seznam uživatelů</h6>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Celé jméno</th>
                                <th scope="col">Email</th>
                                <th scope="col">Role</th>
                                <th scope="col" class="text-right">Akce</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(App\User::all() as $user)
                                <tr>
                                    <th scope="row">{{ $user->id }}</th>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role->displayname }}</td>
                                    <td class="text-right">
                                        <a href="javascript:void(0);"
                                           class="userEditBtn"
                                           data-id="{{ $user->id }}"
                                           data-role="{{ $user->role->name }}"
                                           data-name="{{ $user->name }}"
                                        ><i class="fa fa-search text-primary"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12" @if(!$errors->any()) style="display: none;" @endif id="userEdit">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Úprava uživatele <span id="name_e"></span></h6>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('admin.permissions') }}">
                            @csrf
                            @method('patch')
                            <input type="hidden" id="id_e" name="id_e" value="{{ old('id_e') }}">
                            <div class="form-group">
                                <label for="role_e">Úprava role</label>
                                <select class="form-control{{ $errors->has('role_e') ? ' is-invalid' : '' }}"
                                        id="role_e" name="role_e">
                                    @foreach(App\Role::all() as $role)
                                        <option value="{{ $role->id }}"
                                                id="{{ $role->name }}">{{ $role->displayname }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role_e'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('role_e') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group text-right">
                                <button class="btn btn-primary">Upravit uživatele</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('.userEditBtn').on('click', function () {
            $('#id_e').val($(this).data('id'));
            $('#name_e').text($(this).data('name'));
            @foreach(App\Role::all() as $role)
            $('#{{ $role->name }}').removeAttr('selected');
            @endforeach
            $('#' + $(this).data('role')).attr('selected', '');
            $('#userEdit').show(300);
        });
    </script>
@endsection