@extends('layouts.admin')

@section('title', 'Správa článků')

@section('css')
    <!-- Custom styles for this page -->
    <link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

    <style>
        .table-no-header-border thead th {
            border-top: none;
        }

        .clickable {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Správa článků</h1>
            <a href="{{ route('post.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                        class="fas fa-download fa-plus text-white-50"></i> Nový článek</a>
        </div>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Tvé články</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-no-header-border" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Název</th>
                            <th>Kategorie</th>
                            <th>Datum vytvoření</th>
                            <th>Poslední úprava</th>
                            <th class="text-right">Akce</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{ $post->id }}</td>
                                <td>{{ $post->title }}</td>
                                <td class="text-{{ $post->category->color }}">{{ $post->category->name }}</td>
                                <td>{{ $post->created_at->format('d.m.Y H:i:s') }}</td>
                                <td>{{ $post->updated_at->format('d.m.Y H:i:s') }}</td>
                                <td class="text-right">
                                    <i class="fa fa-trash-alt mr-1 text-danger clickable postDelete" data-toggle="modal" data-target="#deleteModal" data-id="{{ $post->id }}" data-title="{{ $post->title }}"></i>
                                    <a href="{{ url('/admin/post/edit/' . $post->id) }}"><i class="fa fa-search text-primary"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <!-- Logout Modal-->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Opravdu chceš smazat článek?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Pokud si přeješ smazat článek <strong id="delPostTitle"></strong>, zvol "Smazat".</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Zrušit</button>
                    <button class="btn btn-danger" type="button" onclick="$('#postDeleteForm').submit();">Smazat</button>
                    <form action="{{ route('post.delete') }}" method="post" id="postDeleteForm">
                        @csrf
                        @method('delete')
                        <input type="hidden" name="post_id" value="" id="delPostId">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                'order': [[0, 'desc']],
                'columnDefs': [{
                    'targets': 5,
                    'orderable': false
                }],
                "drawCallback": function( settings ) {
                    $('.postDelete').on('click', function() {
                        $('#delPostTitle').text($(this).data('title'));
                        $('#delPostId').val($(this).data('id'));
                    });
                }
            });
        });
    </script>
@endsection