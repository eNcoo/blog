@extends('layouts.admin')

@section('title', 'Nový článek')

@section('css')
    <!-- include summernote css/js -->
    <link href="/vendor/summernote/summernote-bs4.css" rel="stylesheet">
    <link href="/vendor/tagsinput/tagsinput.css" rel="stylesheet">
@endsection


@section('content')
    <div class="container-fluid">
        <form action="{{ route('post.create') }}" method="post">
        @csrf
        @method('put')
        <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Vytvoření článku</h1>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Základní</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="title">Název článku</label>
                                <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" name="title">
                                @if ($errors->has('title'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('title') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="preview">Náhledový text</label>
                                <textarea id="preview" name="preview" class="form-control{{ $errors->has('preview') ? ' is-invalid' : '' }}"
                                          style="resize: none;"></textarea>
                                @if ($errors->has('preview'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('preview') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="preview_image">Adresa náhledové fotografie</label>
                                <input type="url" id="preview_image" name="preview_image" class="form-control{{ $errors->has('preview_image') ? ' is-invalid' : '' }}">
                                @if ($errors->has('preview_image'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('preview_image') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Kategorie a tagy</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="category">Kategorie</label>
                                <select class="form-control{{ $errors->has('category') ? ' is-invalid' : '' }}" id="category" name="category">
                                    <option selected disabled>Prosím, vyber kategorii</option>
                                    @foreach(\App\Category::all() as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('category'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('category') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="tags">Tagy</label>
                                <input type="text" id="tags" name="tags" class="form-control{{ $errors->has('tags') ? ' is-invalid' : '' }}" data-role="tagsinput">
                                @if ($errors->has('tags'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('tags') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Obsah</h6>
                        </div>
                        <div class="card-body">
                            <textarea id="summernote" name="content" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}"></textarea>
                            @if ($errors->has('content'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('content') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <button class="btn btn-primary btn-block">Vytvořit článek</button>
                </div>
        </form>
    </div>
    </div>
@endsection

@section('js')
    <script src="/vendor/summernote/summernote-bs4.min.js"></script>
    <script src="/vendor/tagsinput/tagsinput.js"></script>
    <script>
        $(document).ready(function () {
            $('#summernote').summernote({
                tabsize: 4,
                height: 400
            });
        });
    </script>
@endsection