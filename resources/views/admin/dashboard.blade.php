@php
    $user = Auth::user();
@endphp

@extends('layouts.admin')

@section('title', 'Dashboard')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Hlavní strana</h1>
        </div>

        <!-- Content Row -->
        <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Registrovaní
                                    uživatelé
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                    @if(Auth::user()->hasAccess('admin'))
                                        {{ App\User::all()->count() }}
                                    @else
                                        Je potřeba vyšší oprávnění
                                    @endif
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-user fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Počet článků
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $user->isVerified() ? \App\Post::all()->count() : 'Je potřeba ověření'}}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-book fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Počet kategorií
                                </div>
                                <div class="row no-gutters align-items-center">
                                    <div class="col-auto">
                                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ $user->isVerified() ? \App\Category::all()->count() : 'Je potřeba ověření'}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Počet
                                    tagů
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $user->isVerified() ? \App\Tag::all()->count() : 'Je potřeba ověření'}}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-tags fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @if(!$user->isVerified())
        <!-- Content Row -->
            <div class="row">
                <div class="col-lg-8 mb-4">
                    <!-- Illustrations -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Čekání na ověření</h6>
                        </div>
                        <div class="card-body">
                            <div class="text-center">
                                <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;"
                                     src="/img/undraw_loading_frh4.svg" alt="">
                            </div>
                            <p>
                                Tvůj účet zatím nebyl ověřen.
                                <br>
                                Ověření je důležité, aby si nemohl každý založit účet a psát články bez dozoru.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
    @else
        <!-- Content Row -->
            <div class="row">
                <div class="col-lg-8 mb-4">
                    <!-- Illustrations -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Účet je oveřený</h6>
                        </div>
                        <div class="card-body">
                            <div class="text-center">
                                <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;"
                                     src="/img/undraw_typewriter_i8xd.svg" alt="">
                            </div>
                            <div class="row">
                                <p class="col-8">
                                    Tvůj účet je ověřený.
                                    <br>
                                    Můžeš tedy psát články, tak se do toho pusť.
                                </p>
                                <div class="col-4">
                                    <a href="{{ route('post.create') }}"
                                       style="position: absolute; right: 12px; bottom: 16px;" class="btn btn-primary">Nový
                                        článek</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('js')
    <!-- Page level plugins -->
    <script src="/vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="/js/demo/chart-area-demo.js"></script>
    <script src="/js/demo/chart-pie-demo.js"></script>
@endsection
