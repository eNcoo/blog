<?php

namespace App\Http\Middleware;

use Auth;
use App\Role;
use App\User;
use Closure;

class AccessLevelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $level)
    {
        $id = Role::where('name', $level)->get()->first()->id;
        if(Auth::user()->role_id >= $id) {
            return $next($request);
        } else {
            abort(403);
        }
    }
}
