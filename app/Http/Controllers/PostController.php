<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\Post\StoreRequest;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('posts')->withPosts(Post::orderBy('id', 'desc')->paginate(10));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexByCategory(Category $category)
    {
        return view('posts')->withPosts($category->posts()->orderBy('id',
            'desc')->paginate(10));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexByTag(Tag $tag)
    {
        return view('posts')->withPosts($tag->posts()->orderBy('id',
            'desc')->paginate(10));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexSelf()
    {
        return view('admin.posts.index')->withPosts(Post::where('user_id', Auth::user()->id)->get());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAdmin()
    {
        return view('admin.posts.indexAll')->withPosts(Post::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Post\StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $content = $request->input('content');
        $content = str_replace([
            "<script>",
            "</script>",
            "<?php",
            "?>",
            "<iframe>",
            "</iframe>",
            "onchange=",
            "onclick=",
            "onmouseover=",
            "onmouseout=",
            "onkeydown=",
            "onload="
        ], [
            "&lt;script&gt;",
            "&lt;/script&gt;",
            "&lt;?php;",
            "?&gt;",
            "&lt;iframe&gt;",
            "&lt;/iframe&gt;",
            "onchange&#61;",
            "onclick&#61;",
            "onmouseover&#61;",
            "onmouseout&#61;",
            "onkeydown&#61;",
            "onload&#61;"
        ], $content);

        $post = new Post();
        $post->user_id = Auth::user()->id;
        $post->category_id = $request->category;
        $post->title = $request->title;
        $post->preview = $request->preview;
        $post->content = $content;
        $post->image_url = $request->preview_image;
        $post->save();

        $tags = explode(',', $request->tags);
        foreach ($tags as $tag) {
            $tag = iconv('UTF-8', 'ASCII//TRANSLIT', $tag);
            $tag = preg_replace("/[^a-zA-Z0-9]+/", "", $tag);
            $dbTag = Tag::where('name', $tag);
            if (!$dbTag->exists()) {
                $newTag = new Tag();
                $newTag->name = $tag;
                $newTag->save();
            } else {
                $newTag = $dbTag->first();
            }
            $post->tags()->attach($newTag->id);
        }

        return redirect('/post/' . $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('post')->withPost($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        if (!Auth::user()->hasAccess('admin') && Auth::user()->id != $post->user->id) {
            abort(403);
        }
        return view('admin.posts.edit')->withPost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        if (!Auth::user()->hasAccess('admin') && Auth::user()->id != $post->user->id) {
            abort(403);
        }

        $content = $request->input('content');
        $content = str_replace([
            "<script>",
            "</script>",
            "<?php",
            "?>",
            "<iframe>",
            "</iframe>",
            "onchange=",
            "onclick=",
            "onmouseover=",
            "onmouseout=",
            "onkeydown=",
            "onload="
        ], [
            "&lt;script&gt;",
            "&lt;/script&gt;",
            "&lt;?php;",
            "?&gt;",
            "&lt;iframe&gt;",
            "&lt;/iframe&gt;",
            "onchange&#61;",
            "onclick&#61;",
            "onmouseover&#61;",
            "onmouseout&#61;",
            "onkeydown&#61;",
            "onload&#61;"
        ], $content);

        $post->category_id = $request->category;
        $post->title = $request->title;
        $post->preview = $request->preview;
        $post->content = $content;
        $post->image_url = $request->preview_image;
        $post->save();

        foreach ($post->tags as $tag) {
            $post->tags()->detach($tag->id);
        }

        $tags = explode(',', $request->tags);
        foreach ($tags as $tag) {
            $tag = iconv('UTF-8', 'ASCII//TRANSLIT', $tag);
            $tag = preg_replace("/[^a-zA-Z0-9]+/", "", $tag);
            $dbTag = Tag::where('name', $tag);
            if (!$dbTag->exists()) {
                $newTag = new Tag();
                $newTag->name = $tag;
                $newTag->save();
            } else {
                $newTag = $dbTag->first();
            }
            $post->tags()->attach($newTag->id);
        }

        Session::flash('message', 'Článek byl úspěšně upraven.');
        Session::flash('alert-class', 'success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $post = \App\Post::findOrFail($request->post_id);

        if (Auth::user()->hasAccess('admin') || Auth::user()->id == $post->user_id) {
            $post->delete();
            return redirect()->back();
        }
        abort(403);
    }
}
