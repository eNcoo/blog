<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => [
                "required",
                "min:5",
                "max:50",
                "unique:posts,title"
            ],
            "tags" => [
                "required",
            ],
            "preview" => [
                "required",
                "min:10",
                "max:120",
                "unique:posts,preview"
            ],
            "preview_image" => [
                "required",
                "active_url",
            ],
            "category" => [
                "required",
                "exists:categories,id"
            ],
            "content" => [
                "required",
                "min:100"
            ]
        ];
    }
}
