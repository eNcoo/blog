<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:20',
                'min:2',
                'unique:categories,name'
            ],
            'color' => [
                'required',
                'min:2',
                'max:20'
            ],
            'url' => [
                'required',
                'min:2',
                'max:20',
                'alpha_dash',
                'unique:categories,url'
            ]
        ];
    }
}
