<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_e' => [
                'exists:categories,id'
            ],
            'name_e' => [
                'required',
                'max:20',
                'min:2',
                'unique:categories,name,' . $this->id_e
            ],
            'color_e' => [
                'required',
                'min:2',
                'max:20'
            ],
            'url_e' => [
                'required',
                'min:2',
                'max:20',
                'alpha_dash',
                'unique:categories,url,' . $this->id_e
            ]
        ];
    }
}
