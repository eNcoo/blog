<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    public function getRouteKeyName()
    {
        return 'name';
    }

    public function posts() {
        return $this->belongsToMany(Post::class);
    }
}
