<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/posts', 'PostController@index');
Route::get('/posts/category/{category}', 'PostController@indexByCategory');
Route::get('/posts/tag/{tag}', 'PostController@indexByTag');
Route::get('/post/{post}', 'PostController@show');

Route::group(['prefix' => '/admin', 'middleware' => 'web'], function () {
    Route::group(['middleware' => 'guest'], function () {
        Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('/login', 'Auth\LoginController@login');
        Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
        Route::post('/register', 'Auth\RegisterController@register');
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', function () {
            return redirect('/admin/dashboard');
        });
        Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');
        Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
        Route::post('/logout', 'Auth\LoginController@logout');

        Route::group(['middleware' => 'access:redactor'], function () {
            Route::get('/posts', 'PostController@indexSelf')->name('redactor.posts');
            Route::delete('/post/delete', 'PostController@destroy')->name('post.delete');
            Route::get('/post/create', 'PostController@create')->name('post.create');
            Route::put('/post/create', 'PostController@store');
            Route::get('/post/edit/{post}', 'PostController@edit')->name('post.edit');
            Route::patch('/post/edit/{post}', 'PostController@update');
        });

        Route::group(['middleware' => 'access:admin'], function () {
            Route::get('/posts/all', 'PostController@indexAdmin')->name('admin.posts');
            Route::get('/categories', 'CategoryController@index')->name('admin.categories');
            Route::put('/categories', 'CategoryController@store');
            Route::patch('/categories', 'CategoryController@update');
            Route::get('/permissions', 'UserController@index')->name('admin.permissions');
            Route::patch('/permissions', 'UserController@update');
        });
    });
});
